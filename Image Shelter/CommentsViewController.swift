import UIKit

class CommentsViewController: UIViewController {
    
//MARK: - IBOutlets
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var optionsButton: UIButton!
    
//MARK: - Constants and variables
    var imgArray: [Image]?
    var displayedImage: Image?
    var displayedImageIndex: Int?
    
//MARK: - Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imageView.layer.cornerRadius = 10
        self.assignImage(imageIndex: displayedImageIndex ?? Int())
        
        self.hideKeyboardWhenTappedAround()
        
        let swipeLeftRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeLeftDetected(_:)))
        swipeLeftRecognizer.direction = .left
        self.view.addGestureRecognizer(swipeLeftRecognizer)
        
        let swipeRightRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeRightDetected(_:)))
        swipeRightRecognizer.direction = .right
        self.view.addGestureRecognizer(swipeRightRecognizer)
    }
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.tableViewHeight.constant = self.tableView.contentSize.height
    }
    
//MARK: - IBActions
    @IBAction func swipeLeftDetected(_ sender: UISwipeGestureRecognizer) {
        self.assignImage(imageIndex: self.returnIndexLeftSwipe())
        self.tableView.reloadData()
    }
    
    @IBAction func swipeRightDetected(_ sender: UISwipeGestureRecognizer) {
        self.assignImage(imageIndex: self.returnIndexRightSwipe())
        self.tableView.reloadData()
    }
    
    @IBAction func optionsButtonPressed(_ sender: UIButton) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "SideMenu") as? SideMenu {
            self.addChild(controller)
            controller.view.frame = self.view.frame
            self.view.addSubview(controller.view)
            controller.didMove(toParent: self)
        }
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
//MARK: - Flow functions
    private func assignImage(imageIndex: Int) {
        if let image = imgArray?[imageIndex] {
            self.displayedImage = image
            let imagePath = image.path ?? String()
            let displayedImage = Manager.shared.loadImage(fileName: imagePath)
            self.imageView.image = displayedImage
        }
    }
    
    private func returnIndexRightSwipe() -> Int {
        if let imageArray = self.imgArray,
            var imageIndex = displayedImageIndex {
            imageIndex -= 1
            if imageIndex < 0 {
                imageIndex = imageArray.count - 1
            }
            displayedImageIndex = imageIndex
        }
        return displayedImageIndex ?? Int()
    }
    
    private func returnIndexLeftSwipe() -> Int {
        if let imageArray = self.imgArray,
            var imageIndex = displayedImageIndex {
            imageIndex += 1
            if imageIndex > imageArray.count - 1 {
                imageIndex = 0
            }
            displayedImageIndex = imageIndex
        }
        return displayedImageIndex ?? Int()
    }
    
}

//MARK: - Extensions
extension CommentsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.displayedImage?.commentText.count ?? Int()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? CommentCell else { return UITableViewCell() }
        let comments = self.displayedImage?.commentText
        let dates = self.displayedImage?.commentDate
        cell.configure(with: comments?[indexPath.row] ?? String(), date: dates?[indexPath.row] ?? Date())
        return cell
    }
    
    func tableView(_ tableView: UITableView,
                   leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
        ->   UISwipeActionsConfiguration? {
            if let cell = tableView.cellForRow(at: indexPath) as? CommentCell {
                let editComment = UIContextualAction(style: .normal, title: "Edit") { (action, view, completion) in
                    let alert = UIAlertController(title: "Edit comment", message: "Enter new text", preferredStyle: .alert)
                    
                    alert.addTextField { (textField) in
                        textField.text = cell.commentLabel.text
                    }
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                        let textField = alert?.textFields![0]
                        self.displayedImage?.commentText[indexPath.row] = textField?.text ?? String()
                        cell.commentLabel.text = textField?.text
                        self.tableView.reloadData()
                    }))
                    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    completion(true)
                }
                
                let deleteComment = UIContextualAction(style: .destructive, title: "Delete") { (action, view, completion) in
                    let alert = UIAlertController(title: "Delete comment", message: "Do you wish to delete this comment?", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (_) in
                        self.displayedImage?.commentText.remove(at: indexPath.row)
                        self.displayedImage?.commentDate.remove(at: indexPath.row)
                        self.tableView.beginUpdates()
                        self.tableView.deleteRows(at: [indexPath], with: .automatic)
                        self.tableView.endUpdates()
                    }))
                    alert.addAction(UIAlertAction(title: "No", style: .destructive, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    completion(true)
                }
                
                let configuration = UISwipeActionsConfiguration(actions: [editComment, deleteComment])
                
                return configuration
            } else {
                return UISwipeActionsConfiguration()
            }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
    
    /*@available(iOS 13.0, *)
     func tableView(_ tableView: UITableView,
     contextMenuConfigurationForRowAt indexPath: IndexPath,
     point: CGPoint) -> UIContextMenuConfiguration? {
     if let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? CommentCell {
     
     let editComment = UIAction(title: "Edit", image: UIImage(systemName: "text.bubble")) {_ in
     
     }
     
     let deleteComment = UIAction(title: "Delete", image: UIImage(systemName: "trash"), attributes: .destructive) {_ in
     self.showConfirmation(imageForDeletionIndex: indexPath.row)
     }
     
     let menu = UIMenu(title: "Actions", children: [editComment, deleteComment])
     
     let configuration = UIContextMenuConfiguration(identifier: nil, previewProvider: nil) { (_) -> UIMenu? in
     return menu
     }
     
     return configuration
     }
     }*/
}
