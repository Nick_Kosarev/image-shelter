import UIKit

class ViewController: UIViewController, UITextFieldDelegate, UIGestureRecognizerDelegate {
    
//MARK: - IBOutlets
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var commentField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var layoutView: UIScrollView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var layoutViewBottomConstraint: NSLayoutConstraint!    
    @IBOutlet weak var optionsButton: UIButton!
    
//MARK: - Constants and variables
    let imagePicker = UIImagePickerController()
    var imageCounter = 0
    var imageArray: [Image] = []
    var imageListIndex: Int = 0
    
//MARK: - Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imagePicker.delegate = self
        self.collectionView.delegate = self
        self.hideKeyboardWhenTappedAround()
        self.commentField.delegate = self
        imageArray = Manager.shared.loadImageArray()
        self.assignLikes()
        
        self.saveButton.layer.cornerRadius = 3
        self.collectionView.layer.cornerRadius = 10
        
        let imageNumber = UserDefaults.standard.object(forKey: Keys.imageNumber.rawValue)
        imageCounter = imageNumber as? Int ?? 0
        
        self.registerForKeyboardNotifications()
        
        self.checkElementsAvailability()
        
        guard #available(iOS 13.0, *) else {
            let shortPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(imageShortPressDetected(_:)))
            shortPressRecognizer.delegate = self
            shortPressRecognizer.minimumPressDuration = 0.2
            self.view.addGestureRecognizer(shortPressRecognizer)
            
            let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(imageLongPressDetected(_:)))
            longPressRecognizer.delegate = self
            self.view.addGestureRecognizer(longPressRecognizer)
            return
        }
    }

//MARK: - IBActions
    @IBAction private func imageShortPressDetected(_ sender: UILongPressGestureRecognizer) {
        let touchPoint = sender.location(in: collectionView)
        if let indexPath = self.collectionView.indexPathForItem(at: touchPoint) {
            let cell = self.collectionView.cellForItem(at: indexPath)
            switch sender.state {
            case .began:
                UIView.animate(withDuration: 0.3) {
                    cell?.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                }
            case .ended:
                UIView.animate(withDuration: 0.3) {
                    cell?.transform = CGAffineTransform(scaleX: 1, y: 1)
                }
            default:
                break
            }
        }
    }
    
    @IBAction private func imageLongPressDetected(_ sender: UILongPressGestureRecognizer) {
        let touchPoint = sender.location(in: collectionView)
        if let indexPath = self.collectionView.indexPathForItem(at: touchPoint) {
            switch sender.state {
            case .began:
                self.createSelectionMenu(indexPath: indexPath)
            default:
                break
            }
        }
    }
    
    @IBAction private func optionsButtonPressed(_ sender: UIButton) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "SideMenu") as? SideMenu {
            self.addChild(controller)
            controller.view.frame = self.view.frame
            self.view.addSubview(controller.view)
            controller.didMove(toParent: self)
        }
    }
    
    
    @IBAction private func plusButtonPressed(_ sender: UIButton) {
        self.pick()
    }
    
    @IBAction private func saveButtonPressed(_ sender: UIButton) {
        if commentField.text?.count != 0 {
            let displayedImageIndex = collectionView.currentPage - 1
            let displayedImage = imageArray[displayedImageIndex]
            if let comment = commentField.text {
                let date = Date()
                displayedImage.commentText.append(comment)
                displayedImage.commentDate.append(date)
            }
            Manager.shared.update(item: imageArray)
            commentField.text?.removeAll()
            self.view.endEditing(true)
            self.showCommentAlert()
        }
        
    }
    
    @IBAction private func likeButtonPressed(_ sender: UIButton) {
        let displayedImageIndex = collectionView.currentPage - 1
        let displayedImage = imageArray[displayedImageIndex]
        switch displayedImage.like {
        case true:
            displayedImage.like = false
            sender.isSelected = false
            Manager.shared.update(item: imageArray)
        case false:
            displayedImage.like = true
            sender.isSelected = true
            Manager.shared.update(item: imageArray)
        default:
            displayedImage.like = true
            sender.isSelected = true
            Manager.shared.update(item: imageArray)
        }
    }

//MARK: - Delegate methods
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool{
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if imageArray.isEmpty == false {
            return true
        } else {
            return false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    private func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        imageListIndex = scrollView.currentPage - 1
        self.assignLikes()
        commentField.text?.removeAll()
    }
    
    @objc private func keyboardWillShow(_ notification: NSNotification) {
        let userInfo = notification.userInfo!
        let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            layoutViewBottomConstraint.constant -= keyboardScreenEndFrame.height
            UIView.animate(withDuration: animationDuration) {
                self.view.layoutIfNeeded()
            }
        } else {
            layoutViewBottomConstraint.constant += keyboardScreenEndFrame.height
            UIView.animate(withDuration: animationDuration) {
                self.view.layoutIfNeeded()
            }
        }
    }

//MARK: - Flow functions
    private func createSelectionMenu(indexPath: IndexPath) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let viewCommentsAction = UIAlertAction(title: "Comments", style: .default) { (_) in
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "CommentsViewController") as? CommentsViewController {
                controller.imgArray = self.imageArray
                controller.displayedImageIndex = indexPath.row
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
        let deleteAction = UIAlertAction(title: "Delete", style: .destructive) { (_) in
            self.showConfirmation(imageForDeletionIndex: indexPath.row)
        }
        actionSheet.addAction(viewCommentsAction)
        actionSheet.addAction(deleteAction)
        self.present(actionSheet, animated: true) {
            let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.actionSheetBackgroundTapped))
            actionSheet.view.superview?.subviews[0].addGestureRecognizer(tapRecognizer)
        }
    }
    
    @objc private func actionSheetBackgroundTapped() {
        self.dismiss(animated: true, completion: nil)
        let cells = self.collectionView.visibleCells
        for cell in cells {
            UIView.animate(withDuration: 0.3) {
                cell.transform = CGAffineTransform(scaleX: 1, y: 1)
            }
        }
    }
    
    private func updateCollectionView() {
        imageListIndex = 0
        imageArray = Manager.shared.loadImageArray()
        collectionView.reloadData()
    }
    
    private func checkElementsAvailability() {
        if imageArray.isEmpty == false {
            saveButton.isEnabled = true
            likeButton.isEnabled = true
        } else {
            saveButton.isEnabled = false
            likeButton.isEnabled = false
        }
    }
    
    private func assignLikes() {
        if imageArray.isEmpty == false {
            let displayedImage = imageArray[imageListIndex]
            switch displayedImage.like {
            case false:
                likeButton.isSelected = false
            case true:
                likeButton.isSelected = true
            default:
                likeButton.isSelected = false
            }
        }
    }
    
    private func showCommentAlert() {
        let alert = UIAlertController(title: "Success", message: "The comment is saved", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func showConfirmation(imageForDeletionIndex: Int) {
        let alert = UIAlertController(title: "Confirm the action below", message: "Do you wish to delete this image?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Yes", style: .default) { (_) in
            self.imageArray.remove(at: imageForDeletionIndex)
            Manager.shared.update(item: self.imageArray)
            self.collectionView.reloadData()
            if self.imageArray.count == 1 {
                self.imageListIndex = self.imageArray.count - 1
            }
            self.assignLikes()
            self.checkElementsAvailability()
        }
        alert.addAction(okAction)
        let cancelAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func pick() {
        self.imagePicker.sourceType = .photoLibrary // .photoLibrary/.camera
        present(self.imagePicker, animated: true, completion: nil)
    }
    
}

//MARK: - Extensions
extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let pickedImage = info[.originalImage] as? UIImage {
            let image = pickedImage
            let path = Manager.shared.saveImage(image: image)
            let savedImage = Image()
            imageCounter += 1
            UserDefaults.standard.set(imageCounter, forKey: Keys.imageNumber.rawValue)
            savedImage.imageName = "SavedImage_\(imageCounter)"
            savedImage.path = path
            imageArray.append(savedImage)
            
            Manager.shared.saveImageArray(item: savedImage)

            self.updateCollectionView()
            saveButton.isEnabled = true
            likeButton.isEnabled = true
        }
    }
    
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? CustomCollectionViewCell else { return UICollectionViewCell() }
        
        cell.configure(with: imageArray[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.collectionView.frame.size.width
        let height = self.collectionView.frame.size.height
        return CGSize(width: width, height: height)
    }
    
    @available(iOS 13.0, *)
    func collectionView(_ collectionView: UICollectionView, contextMenuConfigurationForItemAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        let viewComments = UIAction(title: "Comments", image: UIImage(systemName: "text.bubble")) {_ in
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "CommentsViewController") as? CommentsViewController {
                controller.imgArray = self.imageArray
                controller.displayedImageIndex = indexPath.row
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
        let delete = UIAction(title: "Delete", image: UIImage(systemName: "trash"), attributes: .destructive) {_ in
            self.showConfirmation(imageForDeletionIndex: indexPath.row)
        }
        let menu = UIMenu(title: "Actions", children: [viewComments, delete])
        let configuration = UIContextMenuConfiguration(
            identifier: nil,
            previewProvider: { () -> UIViewController? in
                let viewController = UIViewController()
                let imageObject = self.imageArray[indexPath.row]
                let imagePath = imageObject.path
                let image = Manager.shared.loadImage(fileName: imagePath ?? String())
                let imageView = UIImageView(image: image)
                viewController.view = imageView
                viewController.preferredContentSize = imageView.frame.size
                return viewController
        },
            actionProvider: {_ in
                return menu
        })
        return configuration
    }
    
}

