import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var rowImage: UIImageView!
    
    func configure(with object: Image) {
        guard let imagePath = object.path else { return }
        let image = Manager.shared.loadImage(fileName: imagePath)
        rowImage.image = image
    }
    
}
