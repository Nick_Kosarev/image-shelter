import UIKit

class CommentCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.commentView.layer.cornerRadius = self.commentView.frame.size.height / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configure(with comment: String, date: Date) {
        self.commentLabel.text = comment
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d, h:mm a"
        let datestring = formatter.string(from: date)
        self.dateLabel.text = datestring
    }
    

}
