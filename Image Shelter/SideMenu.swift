import UIKit

class SideMenu: UIViewController {
    
//MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sideMenuView: UIView!
    
//MARK: - Constants and variables
    let options: [String] = ["Change password"]

//MARK: - Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.65)
        self.sideMenuView.frame.origin.x = self.view.frame.size.width
        self.startAnimation()
        
        let swipeRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(viewSwiped(_:)))
        swipeRecognizer.direction = .right
        swipeRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(swipeRecognizer)
    }
    
//MARK: - IBActions
    @IBAction func viewTapped(_ sender: UITapGestureRecognizer) {
        self.hide { (_) in
            self.willMove(toParent: nil)
            self.view.removeFromSuperview()
            self.removeFromParent()
        }
    }
    
    @IBAction func viewSwiped(_ sender: UISwipeGestureRecognizer) {
        self.hide { (_) in
            self.willMove(toParent: nil)
            self.view.removeFromSuperview()
            self.removeFromParent()
        }
    }
    
//MARK: - Flow functions
    private func createTappableArea() {
        let tappableView = UIView()
        tappableView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width - self.sideMenuView.frame.size.width, height: self.view.frame.size.height)
        tappableView.backgroundColor = .clear
        self.view.addSubview(tappableView)
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped(_:)))
        tapRecognizer.cancelsTouchesInView = false
        tappableView.addGestureRecognizer(tapRecognizer)
    }
    
    private func hide(completion: ((Bool) -> Void)?) {
        UIView.animate(withDuration: 0.3, animations: {
            self.sideMenuView.frame.origin.x = self.view.frame.size.width
            self.view.alpha = 0.0
        }, completion: completion)
    }
    
    private func startAnimation() {
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.3, animations: {
            self.sideMenuView.frame.origin.x = self.view.frame.size.width - self.sideMenuView.frame.size.width
            self.view.alpha = 1.0
        }) { (_) in
            self.createTappableArea()
        }
    }
}

//MARK: - Extensions
extension SideMenu: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.textColor = .white
        cell.textLabel?.text = options[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ChangePassword") as? ChangePassword {
                self.hide { (_) in
                    self.navigationController?.pushViewController(controller, animated: true)
                    self.willMove(toParent: nil)
                    self.view.removeFromSuperview()
                    self.removeFromParent()
                }
            }
        }
    }
}
