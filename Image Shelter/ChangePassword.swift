import UIKit

class ChangePassword: UIViewController, UITextFieldDelegate {
    
//MARK: - IBActions
    @IBOutlet weak var currentPasswordError: UILabel!
    @IBOutlet weak var newPasswordError: UILabel!
    @IBOutlet weak var confirmPasswordError: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var currentPasswordField: UITextField!
    @IBOutlet weak var newPasswordField: UITextField!
    @IBOutlet weak var confirmPasswordField: UITextField!
    @IBOutlet weak var cancelButton: UIButton!
    
//MARK: - Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        currentPasswordField.delegate = self
        newPasswordField.delegate = self
        confirmPasswordField.delegate = self
        self.hideKeyboardWhenTappedAround()
    }
    
//MARK: - IBActions
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        if currentPasswordField.text?.count == 0 {
            currentPasswordError.text = "Enter current password"
            currentPasswordError.isHidden = false
        }
        
        if newPasswordField.text?.count == 0 {
            newPasswordError.text = "Enter new password"
            newPasswordError.isHidden = false
        }
        if confirmPasswordField.text?.count == 0 {
            confirmPasswordError.text = "Confirm your password"
            confirmPasswordError.isHidden = false
        }
        Manager.shared.getPassword { [weak self] (currentPassword) in
            if self?.newPasswordField.text?.count ?? Int() >= 5 && self?.currentPasswordField.text == currentPassword && self?.confirmPasswordField.text == self?.newPasswordField.text {
                
                guard let newPassword = self?.newPasswordField.text else { return }
                Manager.shared.createPassword(password: newPassword)
                self?.currentPasswordError.isHidden = true
                self?.newPasswordError.isHidden = true
                self?.confirmPasswordError.isHidden = true
                self?.showConfirmation()
            }
        }
    }
    
    
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

//MARK: - Flow functions
    private func showConfirmation() {
        let alert = UIAlertController(title: "Success", message: "New password is saved", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (_) in
            self.navigationController?.popViewController(animated: true)
        }
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
//MARK: - Delegate functions
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == currentPasswordField {
            let currentPasswordText = NSString(string: textField.text ?? String()).replacingCharacters(in: range, with: string)
            if currentPasswordText.count != 0 {
                currentPasswordError.isHidden = true
            }
        }
        
        if textField == newPasswordField {
            let newPasswordText = NSString(string: textField.text ?? String()).replacingCharacters(in: range, with: string)
            if newPasswordText.count >= 5 {
                newPasswordError.isHidden = true
            } else {
                newPasswordError.text = "Minimum length is 5 characters"
                newPasswordError.isHidden = false
            }
        }
        
        if textField == confirmPasswordField {
            let confirmPasswordText = NSString(string: textField.text ?? String()).replacingCharacters(in: range, with: string)
            if confirmPasswordText != newPasswordField.text {
                confirmPasswordError.text = "Passwords do not match"
                confirmPasswordError.isHidden = false
            } else {
                confirmPasswordError.isHidden = true
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
