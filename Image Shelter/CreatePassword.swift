import UIKit

class CreatePassword: UIViewController, UITextFieldDelegate {
    
//MARK: - IBOutlets
    @IBOutlet weak var passwordError: UILabel!
    @IBOutlet weak var confirmPasswordError: UILabel!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var confirmPasswordField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    
//MARK: - Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        passwordField.delegate = self
        confirmPasswordField.delegate = self
        self.hideKeyboardWhenTappedAround()
    }

//MARK: - IBActions
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        if passwordField.text?.count == 0 {
            passwordError.text = "Enter your password"
            passwordError.isHidden = false
        }
        if confirmPasswordField.text?.count == 0 {
            confirmPasswordError.text = "Confirm your password"
            confirmPasswordError.isHidden = false
        }
        if passwordField.text?.count ?? Int() >= 5 && confirmPasswordField.text == passwordField.text {
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController {
                guard let password = passwordField.text else { return }
                Manager.shared.createPassword(password: password)
                passwordError.isHidden = true
                confirmPasswordError.isHidden = true
                let parentVC = self.presentingViewController
                self.dismiss(animated: true) {
                    let navController = UINavigationController(rootViewController: controller)
                    navController.isNavigationBarHidden = true
                    navController.modalPresentationStyle = .fullScreen
                    navController.modalTransitionStyle = .crossDissolve
                    parentVC?.present(navController, animated: true, completion: nil)
                }
            }
        }
    }
    
//MARK: - Delegate methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == passwordField {
            let passwordText = NSString(string: textField.text ?? String()).replacingCharacters(in: range, with: string)
            if passwordText.count >= 5 {
                passwordError.isHidden = true
            } else {
                passwordError.text = "Minimum length is 5 characters"
                passwordError.isHidden = false
            }
        }
        if textField == confirmPasswordField {
            let confirmPasswordText = NSString(string: textField.text ?? String()).replacingCharacters(in: range, with: string)
            if confirmPasswordText != passwordField.text {
                confirmPasswordError.text = "Passwords do not match"
                confirmPasswordError.isHidden = false
            } else {
                confirmPasswordError.isHidden = true
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
}
