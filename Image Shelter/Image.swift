import Foundation

class Image: NSObject, Codable {
    var uuid: String
    var imageName: String?
    var path: String?
    var commentText: [String] = []
    var commentDate: [Date] = []
    var like: Bool?
    
    public enum CodingKeys: String, CodingKey {
        case uuid, imageName, path, commentText, commentDate, like
    }
    
    public override init() {
        uuid = UUID().uuidString // unique ID
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.imageName = try container.decodeIfPresent(String.self, forKey: .imageName)
        self.path = try container.decodeIfPresent(String.self, forKey: .path)
        self.commentText = try container.decodeIfPresent([String].self, forKey: .commentText) ?? []
        self.commentDate = try container.decodeIfPresent([Date].self, forKey: .commentDate) ?? []
        self.like = try container.decodeIfPresent(Bool.self, forKey: .like)
        self.uuid = try container.decodeIfPresent(String.self, forKey: .uuid) ?? String()
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(self.imageName, forKey: .imageName)
        try container.encode(self.path, forKey: .path)
        try container.encode(self.commentText, forKey: .commentText)
        try container.encode(self.commentDate, forKey: .commentDate)
        try container.encode(self.like, forKey: .like)
        try container.encode(self.uuid, forKey: .uuid)
    }
}
