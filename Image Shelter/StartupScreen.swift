import UIKit

class StartupScreen: UIViewController, UITextFieldDelegate {

//MARK: - IBOutlets
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var incorrectPasswordLabel: UILabel!
    
//MARK: - Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        submitButton.layer.cornerRadius = 5
        self.hideKeyboardWhenTappedAround()
        self.passwordField.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        Manager.shared.getPassword { [weak self] (password) in
            print(password)
            if password == "" {
                if let controller = self?.storyboard?.instantiateViewController(withIdentifier: "CreatePassword") as? CreatePassword {
                    self?.present(controller, animated: true, completion: nil)
                }
            }
        }
    }
    
//MARK: - IBActions
    @IBAction func submitButtonPressed(_ sender: UIButton) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController {
            Manager.shared.getPassword { [weak self] (password) in
                if self?.passwordField.text == password {
                    self?.incorrectPasswordLabel.isHidden = true
                    self?.navigationController?.pushViewController(controller, animated: true)
                } else {
                    self?.incorrectPasswordLabel.isHidden = false
                }
            }
        }
            
    }

//MARK: - Delegate functions
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
