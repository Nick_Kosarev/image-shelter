import Foundation
import UIKit
import SwiftyKeychainKit

enum Keys: String {
    case imageNumber = "imageNumber"
}

class Manager {
    
    static let shared = Manager()
    private init() {}
    
    private let defaults = UserDefaults.standard
    private let key = "codable"
    
    
    func getPassword(password: @escaping (String) -> ()) {
        let keychain = Keychain(service: "com.swifty.keychain")
        let accessTokenKey = KeychainKey<String>(key: "accessToken")
        let value = try? keychain.get(accessTokenKey)
        password(value ?? String())
        print(value ?? "")
    }
    
    func createPassword(password: String) {
        let keychain = Keychain(service: "com.swifty.keychain")
        let accessTokenKey = KeychainKey<String>(key: "accessToken")
        try? keychain.set(password, for: accessTokenKey)
    }
    
    
    func update(item: [Image]) {
        defaults.set(encodable: item, forKey: key)
    }
    
    func saveImageArray(item: Image) {
        var imageArray = Manager.shared.loadImageArray()
        imageArray.append(item)
        defaults.set(encodable: imageArray, forKey: key)
    }
    
    func loadImageArray() -> [Image] {
        let item = defaults.value([Image].self, forKey: key)
        return item ?? []
    }
    
    func saveImage(image: UIImage) -> String? {
        //1. Получить директорию
        guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil}
        
        //2. создать уникальное имя файла
        let fileName = UUID().uuidString
        
        //3. Создать путь: директория + имя файла
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        
        //4. Преобразовать UIImage в 0101110100111
        guard let data = image.jpegData(compressionQuality: 1) else { return nil}
        
        //5. Проверяем, есть ли там уже файл, если да - удаляем
        if FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                try FileManager.default.removeItem(atPath: fileURL.path)
                print("Removed old image")
            } catch let removeError {
                print("Couldn't remove file at path", removeError)
            }
        }
        //6. Записываем п.4 по пути п.3
        do {
            try data.write(to: fileURL)
            return fileName
        } catch let error {
            print("Error saving file with error", error)
            return nil
        }
    }
    
    func loadImage(fileName:String) -> UIImage? {
        let documentDirectory = FileManager.SearchPathDirectory.documentDirectory
        
        let userDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)
        
        if let dirPath = paths.first {
            let imageUrl = URL(fileURLWithPath: dirPath).appendingPathComponent(fileName)
            let image = UIImage(contentsOfFile: imageUrl.path)
            return image
        }
        return nil
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}


extension UIScrollView {
    var currentPage: Int {
        return Int((self.contentOffset.x + (0.5 * self.frame.size.width)) / self.frame.width) + 1
    }
}

extension UserDefaults {
    
    func set<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
    }
    
    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
            let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
}

